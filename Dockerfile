FROM alpine:3

RUN apk add --update \
    php7 \
    php7-json \
    php7-phar \
    php7-mbstring \
    php7-openssl \
    php7-fileinfo \
    php7-tokenizer \
    php7-gd \
    php7-dom \
    php7-zip \
    php7-curl \
    php7-xml \
    php7-xmlwriter \
    php7-pdo \
    && rm -rf /var/cache/apk/*

# Get Composer 2.0.7
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/bfd95e2a4383ee9cf7c058c2df29d7acb5f86d77/web/installer -O - -q | php -- --install-dir=/usr/bin --filename=composer --version=2.0.7 --quit