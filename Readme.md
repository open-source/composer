# Composer image
This projects creates a composer Image and stores it in the Container Registry to be used by our projects. It installs commonly used php extensions.  
Cache is stored under `$HOME/.composer`. Be sure to use it.